<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="/welcome" method="post">
        @csrf
        <h2>Buat Account Baru</h2>
        <h3>Sign Up Form</h3>

        <label>First Name :</label>
            <br><br>
            <input name="fname" type="text" >
            <br><br>
        
        <label>Last Name :</label>
            <br><br>
            <input name="lname" type="text" >
            <br><br>

        <label>Gender</label>
        <br><br>

            <input type="radio" name="gender" value="Male"> Male
            <br>
            <input type="radio" name="gender" value="Female"> Female
            <br><br>

        <label>Nationality</label>
        <br><br>

            <select name="nationality">
                <option value="Indonesia">Indonesia</option>
                <option value="Amerika">Amerika</option>
                <option value="Inggris">Inggris</option>
            </select>
            <br><br>
        
        <label>Language Spoken</label>
        <br><br>
            
            <input type="checkbox" name="language"> Bahasa Indonesia
            <br>

            <input type="checkbox" name="language"> English
            <br>

            <input type="checkbox" name="language"> Other
            <br><br>

        <label>Bio</label>
        <br><br>

            <textarea name="bio" cols="30" rows="10"></textarea>
            <br><br>

        <input type="submit" value="SignUp">   
    </form>
</body>
</html>